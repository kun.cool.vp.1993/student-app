import './App.css'
import DepartmentPage from "./pages/DepartmentPage.jsx";
import {Route, Routes} from "react-router-dom";
import StudentPage from "./pages/StudentPage.jsx";
import paths from "./components/constants.js";
import StudentList from "./components/student/StudentList.jsx";
import CreateStudent from "./components/student/CreateStudent.jsx";
import StudentDetail from "./components/student/StudentDetail.jsx";
import LoginPage from "./pages/LoginPage.jsx";
import NotPermissionPage from "./pages/NotPermissionPage.jsx";
import NotFoundPage from "./pages/NotFoundPage.jsx";



function App() {
  return (
    <>

        <Routes>
            <Route path={paths.unAuthenticated} element={<NotPermissionPage />} />
            <Route path={paths.login} element={<LoginPage />} />
            <Route path = {paths.home} element={<DepartmentPage/>} />
            <Route path = {paths.dashboard} element={<DepartmentPage />} />
            <Route path = {paths.student} element={<StudentPage />} >
                <Route index={true} element={<StudentList />} />
                <Route path = {paths.newStudent} element={<CreateStudent />} />
                <Route path = {paths.studentDetail} element={<StudentDetail />} />
                <Route path = {paths.listStudent} element={<StudentList />} />
            </Route>
            <Route path={paths.notFound} element={<NotFoundPage />} />
        </Routes>
    </>
  )
}

export default App
