
import {FaQuestionCircle} from "react-icons/fa";

// eslint-disable-next-line react/prop-types
function NotFound({className = '', children}) {
    return (
        <div className={ `${className} w-100 d-flex flex-column align-items-center justify-content-center`}>
            <FaQuestionCircle size={100} className='text-warning'/>
            <h2 className='mt-2'>Page Not Found</h2>
            <p>Oops! We couldn&#39;t find the page that you&#39;s looking for. </p>
            <p>Please check the address and try again</p>
            {children}
        </div>
    );
}

export default NotFound;