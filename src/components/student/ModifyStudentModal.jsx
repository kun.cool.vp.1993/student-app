import {Button, Modal} from "react-bootstrap";
import PropTypes from "prop-types";
import {useEffect, useState} from "react";
import {api} from "../constants.js";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import dayjs from "dayjs";
import {FaSave, FaTimes} from "react-icons/fa";
import {toast} from "react-toastify";
import {schema} from "./CreateStudent.jsx";

function ModifyStudentModal({show, handleClose, selectedStudent, setStudentId}) {
    const [departments, setDepartments] = useState([]);
    const [currentStudent, setCurrentStudent] = useState(selectedStudent)
    const [loading, setLoading] = useState(true)
    const [newAvatarUrl, setNewAvatarUrl] = useState('')


    useEffect(() => {
        async function getDepartments() {
            try {
                const res = await fetch(api.department);
                const data = await res.json();
                setDepartments(data);
                // setLoading(false)
            } catch (e) {
                console.log(e.message)
            }
        }

        (async () => getDepartments())()
    }, []);

    useEffect(() => {
        setLoading(true)

        async function getStudentById() {
            if (selectedStudent.id) {
                try {
                    let res = await fetch(api.student + `/${selectedStudent.id}`);
                    let data = await res.json();
                    setCurrentStudent(data)
                    if (data) {
                        setValue('fullname', data.fullname)
                        setValue('mobile', data.mobile)
                        setValue('email', data.email)
                        setValue('avatarUrl', data.avatarUrl)
                        setValue('dob', dayjs(data.dob).format('YYYY-MM-DD'))
                        setValue('avatarUrl', data.avatarUrl)
                        setValue('department', JSON.stringify(data.department))
                        setValue('gender',data.gender)
                    }

                    setLoading(false)
                } catch (e) {
                    console.log(e.messages)
                }
            }

        }

        (async () => getStudentById())()

    }, [selectedStudent.id]);

    const {
        register,
        handleSubmit,
        // reset,
        setValue,
        formState: {errors}
    } = useForm({
        resolver: yupResolver(schema)
    });
    const handleCloseModal = () => {
        setNewAvatarUrl(null);
        handleClose(false)
    }
    const onSubmit = (studentData) => {
        let departmentObj = {}
        try {
            // disableCreateButtonRef.current.disabled = true
            // disableCreateButtonRef.current.innerHTML = 'Creating...'
            departmentObj = JSON.parse(studentData.department)
        } catch (err) {
            console.log(err.message)
            toast.error('something went wrong with department field')
            return
        }

        studentData = {
            ...studentData,
            department: departmentObj
        }
        fetch(api.student + `/${selectedStudent.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(studentData)
        })
            .then(res => res.json())
            .then(data => {
                if(Object.keys(data).length){
                    toast.success('Student added successfully', {theme: "colored"})
                    handleClose(false)
                    setNewAvatarUrl('')
                    setStudentId(null)
                }else{
                    toast.error('student added failed', {theme: "colored"})
                }
                // reset();

                // navigate(api.student)
            })
            .catch(err => {
                    console.log(err.message)
                    toast.error('Something went wrong, Student adding failed!')
                }
            )
            .finally(() => {
                // disableCreateButtonRef.current.disabled = false
                // disableCreateButtonRef.current.innerHTML = 'Create'
            })
    }
    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            size='xl'
        >
            <Modal.Header closeButton>
                <Modal.Title>Edit {currentStudent.fullname} &#39;s information</Modal.Title>
            </Modal.Header>

            <form onSubmit={handleSubmit(onSubmit)}>
                <Modal.Body>
                    {
                        loading ? <div>loading</div> :
                            <div className='row'>
                                <div className='col-md-5 col-lg-5 col-sm-12'>
                                    <div className='form-group'>
                                        <label className='form-label'>Full name</label>
                                        <input
                                            type='text'
                                            {...register('fullname')}
                                            className={`form-control ${errors.fullname && "is-invalid"}`}
                                            //className={`form-control`}
                                            placeholder='Fullname...'
                                        />
                                        <p className='invalid-feedback'>{errors.fullname?.message}</p>
                                    </div>
                                    <div className='row'>
                                        <div className='form-group mt-2 col-md-6 col-lg-6 col-sm-12'>
                                            <label className='form-label'>Date of birth</label>
                                            <input
                                                type='date'
                                                {...register('dob')}
                                                className={`form-control ${errors.dob && "is-invalid"}`}
                                                //className={`form-control`}
                                            />
                                            <p className='invalid-feedback'>{errors.dob?.message}</p>
                                        </div>
                                        <div className='form-group mt-2 col-md-6 col-lg-6 col-sm-12'>
                                            <label className='form-label'>Gender</label>
                                            <div className='mt-2'>
                                                {currentStudent.gender ?
                                                    <>
                                                        <div className="form-check form-check-inline">
                                                            <input
                                                                className={`form-check-input ${errors.gender && "is-invalid"}`}
                                                                //className={`form-check-input`}
                                                                {...register('gender')}
                                                                value={true.toString()}
                                                                type="radio"
                                                                checked={true}
                                                            />
                                                            <label className="form-check-label">
                                                                Male
                                                            </label>
                                                        </div>
                                                        <div className="form-check form-check-inline">
                                                            <input
                                                                className={`form-check-input ${errors.gender && "is-invalid"}`}
                                                                //className={`form-check-input`}
                                                                {...register('gender')}
                                                                value={false.toString()}
                                                                type="radio"

                                                            />
                                                            <label className="form-check-label">
                                                                Female
                                                            </label>
                                                        </div>
                                                    </>
                                                    :
                                                    <>
                                                        <div className="form-check form-check-inline">
                                                            <input
                                                                className={`form-check-input ${errors.gender && "is-invalid"}`}
                                                                //className={`form-check-input`}
                                                                {...register('gender')}
                                                                value={true.toString()}
                                                                type="radio"
                                                                // checked={currentStudent?.gender}
                                                            />
                                                            <label className="form-check-label">
                                                                 Male
                                                            </label>
                                                        </div>
                                                        <div className="form-check form-check-inline">
                                                            <input
                                                                className={`form-check-input ${errors.gender && "is-invalid"}`}
                                                                //className={`form-check-input`}
                                                                {...register('gender')}
                                                                value={false.toString()}
                                                                type="radio"
                                                                checked={true}
                                                            />
                                                            <label className="form-check-label">
                                                                Female
                                                            </label>
                                                        </div>
                                                    </>
                                                }
                                                <p className='invalid-feedback d-block'>{errors.gender?.message}</p>
                                            </div>
                                        </div>
                                        <div className='form-group mt-2'>
                                            <label className='form-label'>Mobile</label>
                                            <input
                                                type='tel'
                                                {...register('mobile')}
                                                className={`form-control ${errors.mobile && "is-invalid"}`}
                                                //className={`form-control`}
                                                placeholder='Mobile...'
                                            />
                                            <p className='invalid-feedback'>{errors.mobile?.message}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className='col-md-4 col-lg-4 col-sm-12'>
                                    <div className='form-group'>
                                        <label className='form-label'>Email</label>
                                        <input
                                            type='email'
                                            {...register('email')}
                                            className={`form-control ${errors.email && "is-invalid"}`}
                                            //className={`form-control`}
                                            placeholder='Email...'
                                        />
                                        <p className='invalid-feedback'>{errors.email?.message}</p>
                                    </div>
                                    <div className='form-group mt-2'>
                                        <div className='mt-2'>
                                            <label className='form-label'>Department</label>
                                        </div>
                                        <select className={`form-select ${errors.department && "is-invalid"}`}
                                                {...register('department')}
                                        >
                                            {departments?.map(item =>
                                                <option key={item.id} value={JSON.stringify(item)}>
                                                    {item.name}
                                                </option>
                                            )}
                                        </select>
                                        <p className='invalid-feedback'>{errors.department?.message}</p>
                                    </div>
                                    <div className='form-group mt-2'>
                                        <label className='form-label'>Avatar URL</label>
                                        <input
                                            type='url'
                                            {...register('avatarUrl')}
                                            className={`form-control ${errors.avatarUrl && "is-invalid"}`}
                                            //className={`form-control`}
                                            placeholder='Avatar URL...'

                                            onChange={e => setNewAvatarUrl(e.target.value)}
                                        />
                                        <p className='invalid-feedback'>{errors.avatarUrl?.message}</p>
                                    </div>
                                </div>
                                <div className='col-md-3 col-lg-3 col-sm-12'>
                                    <img
                                        src={newAvatarUrl || currentStudent.avatarUrl}
                                        alt=''
                                        className='w-100 rounded-3'
                                    />
                                </div>
                            </div>
                    }

                </Modal.Body>
                <Modal.Footer>
                    <Button className='d-flex align-items-center' type='button' variant="secondary"
                            onClick={() => handleCloseModal()}>
                        <FaTimes className='me-2'/>
                        <span>Close</span>
                    </Button>
                    <button type='submit'  className='d-flex align-items-center btn btn-primary'>
                        <FaSave className='me-2'/>
                        <span>Save</span>
                    </button>
                </Modal.Footer>
            </form>
        </Modal>
    );
}

ModifyStudentModal.propTypes = {
    show: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    selectedStudent: PropTypes.object.isRequired
}
export default ModifyStudentModal;