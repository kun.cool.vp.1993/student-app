import {Link, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import paths, {api} from "../constants.js";
import {FaBackward, FaBirthdayCake, FaMobileAlt, FaUser} from "react-icons/fa";
import {MdEmail} from "react-icons/md";
import dayjs from "dayjs";
import {BsGenderAmbiguous} from "react-icons/bs";
import {FcDepartment} from "react-icons/fc";
import NotFound from "../exceptions/NotFound.jsx";

function StudentDetail() {
    const {studentid} = useParams();
    const [student, setStudent] = useState({});
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        setLoading(true)

        async function getStudentById() {
            let studentRes = await fetch(api.student + `/${studentid}`);
            let data = await studentRes.json();
            setStudent(data)
            setLoading(false)
        }

        (async()=>getStudentById())()
    }, [studentid]);
    if (!studentid || loading) {
        return <div>loading...</div>

    }
    if(student==='Not found'){
        return (<>
            <NotFound className='mt-4'>
                <div className='mt-2'>
                    <Link to={paths.student}><FaBackward className='me-1'/> Back to list!</Link>
                </div>
            </NotFound>
        </>)
    }
    return (
        <>
            <div className='d-flex align-items-center mt-2'>
                <div className=''>
                <img className='avatar-lg me-4'
                         src={student.avatarUrl}
                         alt=''
                    />
                </div>
                <div className='flex-grow-1 d-flex flex-column'>
                    <div className='border-dashed  py-2'>
                        <FaUser size={20} className='text-primary align-items-center me-2'/>
                        <span>{student.fullname}</span>
                    </div>
                    <div className='border-dashed py-2'>
                        <MdEmail size={20} className='text-primary align-items-center me-2'/>
                        <span>{student.email}</span>
                    </div>
                    <div className='border-dashed py-2'>
                        <FaBirthdayCake size={20} className='text-primary align-items-center me-2 '/>
                        <span>{dayjs(student.dob).format('MMMM DD YYYY')}</span>
                    </div>
                    <div className='border-dashed  py-2'>
                        <BsGenderAmbiguous size={20} className='text-primary align-items-center me-2'/>
                        <span>{student.gender ? 'Male' : 'Female'}</span>
                    </div>
                    <div className='border-dashed  py-2'>
                        <FaMobileAlt size={20} className='text-primary align-items-center me-2'/>
                        <span>{student.mobile}</span>
                    </div>
                    <div className='border-dashed  py-2'>
                        <FcDepartment size={20} className='text-primary me-2'/>
                        <span>{student.department?.name}</span>
                    </div>
                </div>

            </div>
            <div className='mt-2'>
                <Link  to={paths.student}><FaBackward className='me-1' /> Back to list!</Link>
            </div>
        </>
    );
}

export default StudentDetail;