import {useEffect, useState} from "react";
import dayjs from "dayjs";
import {PiGenderFemaleBold, PiGenderMaleBold} from "react-icons/pi";
import {MdDelete} from "react-icons/md";
import Swal from "sweetalert2";
import swalWithBootstrapButtons from "sweetalert2";
import {Link} from "react-router-dom";
import {api} from "../constants.js";
import {FaUserCog} from "react-icons/fa";
import ModifyStudentModal from "./ModifyStudentModal.jsx";
import Spinner from "../spinner/Spinner.jsx";

function StudentList() {

    const [studentList, setStudentList] = useState([]);
    const [selectedStudent, setSelectedStudent] = useState({})
    const [show, setShow] = useState(false);
    const [studentId, setStudentId] = useState(null)
    useEffect(() => {
        function getStudentList() {
            fetch(api.student)
                .then(res => res.json())
                .then(data => setStudentList(data))
                .catch(error => console.log(error.messages))
        }

        getStudentList();
    }, [selectedStudent,studentId]);

    const handleDelete = (id) => {
        Swal.fire({
            title: "Are you sure to delete?",
            text: "You won't be able to revert this!",
            // icon: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancel",
            confirmButtonText: "Confirm",
            reverseButtons: true
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const res = await fetch(`${api.student}/${id}`, {method: 'DELETE'})
                    const data = await res.json();
                    setSelectedStudent(data)
                    await swalWithBootstrapButtons.fire({
                        title: "Deleted!",
                        text: "student information have been deleted.",
                        // icon: "success"
                    })

                } catch (e) {
                    await swalWithBootstrapButtons.fire({
                        title: "Something went wrong!",
                        text: "Your file failed to deleted, please contact to administrator.",
                        // icon: "error"
                    })
                }
            }
        })
    }
    const handleEditInfo = (student) => {
        setShow(true)
        setSelectedStudent(student)
        setStudentId(student.id)
    }
    if (studentList.length === 0) {
        return <Spinner />
    }
    return (
        <>
            <table className='table table-bordered table-striped table-hover rounded-3 overflow-hidden'>
                <caption>

                </caption>
                <thead className='table-secondary'>
                <tr>
                    <th className='text-center'>#ID</th>
                    <th className='text-center'>Full name</th>
                    <th className='text-center'>Date of birth</th>
                    <th className='text-center'>Email</th>
                    <th className='text-center'>Mobile</th>
                    <th className='text-center'>Department</th>
                    <th className='text-center'>Action</th>
                </tr>
                </thead>
                <tbody>
                {studentList.map(item => (
                    <tr key={item.id}>
                        <td className='align-middle'>{item.id}</td>
                        <td>
                            <div className='d-flex align-items-center'>
                                <img
                                    className='avatar-sm'
                                    src={item.avatarUrl}
                                    alt={''}
                                />
                                <div className='d-flex flex-column'>
                                    <Link to={`/student/detail/${item.id}`} className='text-dark'>
                                        {item.fullname}
                                    </Link>
                                    {item.gender ? <PiGenderMaleBold size={16} color={'#0d6efd'}/> :
                                        <PiGenderFemaleBold size={16} color={'#dc3545'}/>}
                                </div>
                            </div>
                        </td>
                        <td className='align-middle text-end'>
                            {dayjs(item.dob).format('MMM DD YYYY')}
                        </td>
                        <td className='align-middle text-end'>
                            {item.email}
                        </td>
                        <td className='align-middle text-end'>
                            {item.mobile}
                        </td>
                        <td className='align-middle'>
                            {item.department.name}
                        </td>
                        <td className='align-middle'>
                            <MdDelete title='Remove student' className='me-2' size={20} color={'#dc3545'} role='button'
                                      onClick={() => handleDelete(item.id)}/>
                            <FaUserCog role='button' title='Edit Information' size={20} color={'#0d6efd'}
                                       onClick={() => handleEditInfo(item)}/>

                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
            <ModifyStudentModal handleClose={setShow} show={show} selectedStudent = {selectedStudent} setStudentId = {setStudentId}/>
        </>
    );
}

export default StudentList;
