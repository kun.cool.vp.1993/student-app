import {useEffect, useRef, useState} from "react";
import {FaUserPlus, FaUserTimes} from "react-icons/fa";
import {useForm} from "react-hook-form";
import * as yup from 'yup'
import {yupResolver} from "@hookform/resolvers/yup";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {useNavigate} from "react-router-dom";
import  {api} from "../constants.js";

export const schema = yup
    .object({
        fullname: yup.string().required(),
        dob: yup.date().required().typeError('Date of birth is required field'),
        gender: yup.boolean().required(),
        mobile: yup.string().required(),
        email: yup.string().email().required(),
        department: yup.string().required(),
        avatarUrl: yup.string().url().required()
    })
    .required()

function CreateStudent() {
    const [departments, setDepartments] = useState([]);
    const {
        register,
        handleSubmit,
        reset,
        formState: {errors}
    } = useForm({
        resolver: yupResolver(schema)
    });

    const navigate = useNavigate();

    const disableCreateButtonRef = useRef();
    useEffect(() => {
        async function getDepartments() {
            try {
                const res = await fetch(api.department);
                const data = await res.json();
                setDepartments(data);
            } catch (e) {
                console.log(e.message)
            }
        }
        // Fix warning Promise returned from getDepartments is ignored
        (async ()=> {
            await getDepartments()
        })()
    }, []);

    const handleCreateStudent = (studentData) => {

        let departmentObj = {}
        try {
            disableCreateButtonRef.current.disabled = true
            disableCreateButtonRef.current.innerHTML = 'Creating...'
            departmentObj = JSON.parse(studentData.department)
        } catch (err) {
            console.log(err.message)
            toast.error('something went wrong with department field')
            return
        }

        studentData = {
            ...studentData,
            department: departmentObj
        }
        fetch(api.student, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(studentData)
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                reset();
                toast.success('Student added successfully', {theme: "colored"})

                navigate(api.student)
            })
            .catch(err => {
                    console.log(err.message)
                    toast.error('Something went wrong, Student adding failed!')
                }
            )
            .finally(() => {
                disableCreateButtonRef.current.disabled = false
                disableCreateButtonRef.current.innerHTML = 'Create'
            })
    }
    return (
        <form
            onSubmit={handleSubmit(handleCreateStudent)}
            className='border rounded p-3'>
            <div className='row'>
                <div className='col-md-6 col-lg-6 col-sm-12'>
                    <div className='form-group'>
                        <label className='form-label'>Full name</label>
                        <input
                            type='text'
                            {...register('fullname')}
                            className={`form-control ${errors.fullname && "is-invalid"}`}
                            placeholder='Fullname...'
                        />
                        <p className='invalid-feedback'>{errors.fullname?.message}</p>
                    </div>
                    <div className='row'>
                        <div className='form-group mt-2 col-md-6 col-lg-6 col-sm-12'>
                            <label className='form-label'>Date of birth</label>
                            <input
                                type='date'
                                {...register('dob')}
                                className={`form-control ${errors.dob && "is-invalid"}`}
                            />
                            <p className='invalid-feedback'>{errors.dob?.message}</p>
                        </div>
                        <div className='form-group mt-2 col-md-6 col-lg-6 col-sm-12'>
                            <label className='form-label'>Gender</label>
                            <div className='mt-2'>
                                <div className="form-check form-check-inline">
                                    <input
                                        className={`form-check-input ${errors.gender && "is-invalid"}`}
                                        {...register('gender')}
                                        value={true.toString()}
                                        type="radio"
                                    />
                                    <label className="form-check-label">
                                        Male
                                    </label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input
                                        className={`form-check-input ${errors.gender && "is-invalid"}`}
                                        {...register('gender')}
                                        value={false.toString()}
                                        type="radio"
                                    />
                                    <label className="form-check-label">
                                        Female
                                    </label>
                                </div>
                                <p className='invalid-feedback d-block'>{errors.gender?.message}</p>
                            </div>
                        </div>
                        <div className='form-group'>
                            <label className='form-label'>Mobile</label>
                            <input
                                type='tel'
                                {...register('mobile')}
                                className={`form-control ${errors.mobile && "is-invalid"}`}
                                placeholder='Mobile...'
                            />
                            <p className='invalid-feedback'>{errors.mobile?.message}</p>
                        </div>
                    </div>
                </div>
                <div className='col-md-6 col-lg-6 col-sm-12'>
                    <div className='form-group'>
                        <label className='form-label'>Email</label>
                        <input
                            type='email'
                            {...register('email')}
                            className={`form-control ${errors.email && "is-invalid"}`}
                            placeholder='Email...'
                        />
                        <p className='invalid-feedback'>{errors.email?.message}</p>
                    </div>
                    <div className='form-group mt-2'>
                        <div className='mt-2'>
                            <label className='form-label'>Department</label>
                        </div>
                        <select className={`form-select ${errors.department && "is-invalid"}`}
                                {...register('department')}
                                defaultValue=''>
                            <option value='' disabled={true}>
                                Please select your department
                            </option>
                            {departments?.map(item =>
                                <option key={item.id} value={JSON.stringify(item)}>
                                    {item.name}
                                </option>
                            )}
                        </select>
                        <p className='invalid-feedback'>{errors.department?.message}</p>
                    </div>
                    <div className='form-group mt-2'>
                        <label className='form-label'>Avatar URL</label>
                        <input
                            type='url'
                            {...register('avatarUrl')}
                            className={`form-control ${errors.avatarUrl && "is-invalid"}`}
                            placeholder='Avatar URL...'
                        />
                        <p className='invalid-feedback'>{errors.avatarUrl?.message}</p>
                    </div>
                    <div className='d-flex justify-content-end mt-3'>
                        <button type='submit' ref={disableCreateButtonRef} className=' btn btn-sm btn-success w-25'>
                            <FaUserPlus className='me-1'/>
                            Creat
                        </button>
                        <button
                            type='button'
                            className='btn btn-sm btn-secondary w-25 ms-2'
                            onClick={() => reset()}
                        >
                            <FaUserTimes className='me-1'/>
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </form>
    );
}

export default CreateStudent;