

function Spinner() {
    return (
        <div className='spinner-loading'>
            <div className="spinner-border text-danger" role="status">
            </div>
            <div className="spinner-border text-warning" role="status">
            </div>
            <div className="spinner-border text-info" role="status">
            </div>
        </div>
    );
}

export default Spinner;