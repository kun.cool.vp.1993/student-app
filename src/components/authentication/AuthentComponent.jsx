import {useNavigate} from "react-router-dom";
import {getToken} from "../../utilities/helper.js";
import paths from "../constants.js";
import {useEffect} from "react";

const AuthentComponent = () => {
    const navigate = useNavigate()
    let cookie = getToken();
    useEffect(() => {

        if(!cookie?.student_app_token){
            navigate(paths.unAuthenticated, {replace: true})
        }
    }, [cookie]);
    return (
        <></>
    );
}

export default AuthentComponent;