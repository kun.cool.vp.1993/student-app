
function Footer() {
    return (
        <div className='container'>
            <footer className='py-3'>
                Copyright &copy; {(new Date).getFullYear()}
            </footer>
        </div>
    );
}

export default Footer;