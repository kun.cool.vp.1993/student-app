const paths = {
    dashboard: '/dashboard',
    student: '/student',
    listStudent: 'list',
    newStudent: 'add',
    studentDetail: 'detail/:studentid',
    home: '/',
    login: '/login',
    notPermission: '/not-permission',
    notFound: '*'
}

export const api = {
    student: import.meta.env.VITE_API_URI + 'student',
    department: import.meta.env.VITE_API_URI + 'department',
    login: import.meta.env.VITE_GET_TOKEN
}

export default paths;