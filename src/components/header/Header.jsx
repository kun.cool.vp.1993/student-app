import {PiStudentBold} from "react-icons/pi";
import {FaSignOutAlt} from "react-icons/fa";
import {Link, useNavigate} from "react-router-dom";
import paths from "../constants.js";


function Header() {
    const navigate = useNavigate()

    return (
        <nav className='navbar navbar-expand-lg border-bottom'>
            <div className='container text-dark'>
                <Link to={paths.home} className='navbar-brand d-flex align-items-center'>
                    <PiStudentBold size={30} className='me-2'/>
                    Student App
                </Link>
                <button
                    onClick={()=>navigate(paths.login)}
                    className='btn btn-outline-secondary btn-sign-out d-flex align-items-center'>
                    <FaSignOutAlt
                        size={18}
                        className='me-2'

                    />
                    Sign out
                </button>
            </div>
        </nav>
    );
}

export default Header;