import {AiFillDashboard} from "react-icons/ai";
import {PiStudentFill} from "react-icons/pi";
import {NavLink} from "react-router-dom";
import paths from "../constants.js";

function Sidebar() {
    return (
        <aside className='me-3' style={{minWidth: '200px',
        minHeight: '500px'}}>
            <ul className='nav flex-column'>
                <li className='nav-item'>
                    <NavLink to={paths.dashboard} className='nav-link d-flex align-items-center'>
                        <AiFillDashboard size={20} className={'me-2'}/>
                        Dashboard
                    </NavLink>
                </li>
                <li className='nav-item'>
                    <NavLink to={paths.student} className='nav-link d-flex align-items-center'>
                        <PiStudentFill size={20} className={'me-2'} />
                        Student
                    </NavLink>
                </li>
            </ul>
        </aside>
    );
}

export default Sidebar;