import MainLayout from "../layouts/MainLayout.jsx";
import Department from "../components/department/Department.jsx";

function DepartmentPage() {
    return (
        <MainLayout>
            <Department/>
        </MainLayout>
    );
}

export default DepartmentPage;