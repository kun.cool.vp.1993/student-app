import {Link} from "react-router-dom";
import paths from "../components/constants.js";
import { FaTimesCircle} from "react-icons/fa";


function NotPermissionPage() {
    return (
        <div className='vh-100 w-100 d-flex flex-column align-items-center justify-content-center'>
            <FaTimesCircle size={100} className='text-danger'/>
            <h2 className='mt-2'>Access Denied </h2>
            <p>You do not have permission to access this page</p>
            <p>Please <Link to={paths.login}>Sign in</Link> and try again</p>
        </div>
    );
}

export default NotPermissionPage;