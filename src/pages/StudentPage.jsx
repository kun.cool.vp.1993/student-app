import MainLayout from "../layouts/MainLayout.jsx";
import {NavLink, Outlet, useLocation, useParams} from "react-router-dom";
import paths from "../components/constants.js";
import {MdGroup, MdPersonAdd} from "react-icons/md";
import {BiSolidUserDetail} from "react-icons/bi";
import AccountLayout from "../layouts/AccountLayout.jsx";

function StudentPage() {
    const location = useLocation();
    const {studentid} = useParams();

    const path = location.pathname.split('/').pop();
    const isActived = path === 'student';

    return (
        <MainLayout>
            <ul className='nav nav-tabs'>
                <li className='nav-item'>
                    <NavLink to={paths.listStudent} className={`nav-link ${isActived ? 'active': ''}` } >
                        <MdGroup size={20} className='me-1'/>
                        Student List
                    </NavLink>
                </li>
                <li className='nav-item'>
                    <NavLink to={paths.newStudent} className={`nav-link`}>
                        <MdPersonAdd size={20} className='me-1'/>
                        Add New
                    </NavLink>
                </li>
                {studentid && <li className='nav-item'>
                    <NavLink to={paths.studentDetail.replace(':studentid',studentid)} className='nav-link'>
                        <BiSolidUserDetail size={20} className='me-1'/>
                        Student Detail
                    </NavLink>
                </li>}
            </ul>
            <Outlet />
            <AccountLayout />
        </MainLayout>
    );
}

export default StudentPage;