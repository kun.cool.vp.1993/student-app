
import {FaFastBackward} from "react-icons/fa";
import {useNavigate} from "react-router-dom";
import NotFound from "../components/exceptions/NotFound.jsx";


function NotFoundPage() {
    const navigate = useNavigate()
    return (
        <NotFound className='vh-100'>
            <button
                className='btn btn-outline-warning d-flex align-items-center'
                onClick={() => navigate(-1)}
            >
                <FaFastBackward
                    size={15}
                    className='me-2'
                />
                Back to previous page
            </button>
        </NotFound>
    );
}

export default NotFoundPage;