
import AccountLayout from "../layouts/AccountLayout.jsx";
import {FaUserShield} from "react-icons/fa";
import {Link, useNavigate} from "react-router-dom";
import * as  yup from "yup";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import paths, {api} from "../components/constants.js";
import {toast} from "react-toastify";
import {useState} from "react";


const schema = yup.object({
    email: yup.string().email().required(),
    password: yup.string().required()
})
function LoginPage() {

    const navigate = useNavigate()
    const [loading, setLoading] = useState(false)

    const {register, handleSubmit,formState: {errors}} = useForm({
        resolver: yupResolver(schema),
        defaultValues:{
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"
        }
    })

    const handleLogin = async (loginInfo) => {
        try{
            setLoading(true)
            let loginRes = await fetch(api.login,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body:JSON.stringify(loginInfo)
            })
            let data = await loginRes.json();
            if(data?.token){
                let date = new Date();
                let expires = date.setMinutes(date.getMinutes() + 1);
                let expiresUTCtime  = (new Date(expires)).toUTCString()
                document.cookie = `student_app_token=${data.token};expires=${expiresUTCtime}`
                toast.success('Login succeed')
                navigate(paths.home, {replace: true})
            }else if(data?.error){
                toast.error('Email or password incorrect')
            }

        }
        catch (e) {
            toast.error(e.message)
        }
        finally {
            setLoading(false)
        }
    }

    return (
        <AccountLayout>
            <div className='login-page bg-light d-flex flex-column align-items-center justify-content-center'>
                <h6>
                    <FaUserShield size={40} className='me-2'/>
                    Student App
                </h6>
                <form onSubmit={handleSubmit(handleLogin)} className='container'>
                    <div className='form-group'>
                        <label className='form-label'>Email</label>
                        <input
                            type='email'
                            {...register('email')}
                            className={`form-control ${errors.email && "is-invalid"}`}
                        />
                        <p className='invalid-feedback'>{errors.email?.message}</p>
                    </div>
                    <div className='form-group my-2'>
                    <label className='form-label'>Password</label>
                        <input
                            type='password'
                            {...register('password')}
                            className={`form-control ${errors.password && "is-invalid"}`}
                        />
                        <p className='invalid-feedback'>{errors.password?.message}</p>
                    </div>
                    <button
                        className='btn btn-primary mt-4 mb-2 w-100'
                        type='submit'
                        disabled={loading}
                    >
                        {loading?'Logging...': 'Log In'}
                    </button>
                    <Link className='mb-3' to={'#'}>Forgot password</Link>
                </form>
            </div>
        </AccountLayout>
    );
}

export default LoginPage;