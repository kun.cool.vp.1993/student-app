export const getToken = () => {
    const cookieArr = document.cookie.split(';').map(item => item.split('='));
    return cookieArr.reduce(
        (obj,
         [key, value]) =>
            ({
                ...obj,
                [key.trim()]: value
            })
        ,{}
    )

}