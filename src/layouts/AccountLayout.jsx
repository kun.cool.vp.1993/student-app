import PropTypes from "prop-types";


function AccountLayout({children}) {
    return (
        <div className='vh-100 d-flex align-items-center justify-content-center'>
            {children}
        </div>
    );
}

AccountLayout.propTypes = {
    children: PropTypes.node.isRequired
}
export default AccountLayout;