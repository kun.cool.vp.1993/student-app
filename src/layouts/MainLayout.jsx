
import Header from "../components/header/Header.jsx";
import Sidebar from "../components/sidebar/Sidebar.jsx";

import Footer from "../components/footer/Footer.jsx";
import PropTypes from "prop-types";
import {useNavigate} from "react-router-dom";
import {getToken} from "../utilities/helper.js";
import {useEffect} from "react";
import paths from "../components/constants.js";

function MainLayout({children}) {
    const navigate = useNavigate()
    let cookie = getToken();
    useEffect(() => {

        if(!cookie?.student_app_token){
            navigate(paths.notPermission, {replace: true})
        }
    }, [cookie]);
    return (
        <div className='container'>
            <Header/>
            <div className='d-flex'>
                <Sidebar/>
                <main className='flex-grow-1'>
                    {children}
                </main>
            </div>
            <Footer/>
        </div>
    );
}

MainLayout.propTypes = {
    children: PropTypes.node.isRequired
}

export default MainLayout;